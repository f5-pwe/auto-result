FROM alpine

ENV RESULT="success" \
    RESULT_CODE=0 \
    MESSAGE="auto success" \
    KOG_ENV_CONTEXT=true

CMD ["/do-it.sh"]
COPY do-it.sh /do-it.sh
